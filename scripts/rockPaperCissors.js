const possibleMoves = ["rock", "paper", "cissors"];

game();

function game(){
    let playerScore = 0;
    let computerScore = 0;
    for (let i = 1; i <= 5; i++){
        let playerMove = playerMoveChoice();
        let computerMove = computerMoveChoice();

        console.log("You choose " + playerMove);
        console.log("The computer choose " + computerMove);

        let result = playRound(playerMove, computerMove);
        if(result != undefined){
            if(result){
                console.log("you win the round");
                playerScore++;
            }
            else{
                console.log("you loose the round");
                computerScore++;
            }
        }
        else{
            console.log("ex-aequo !");
        }
    }
    displayResult(playerScore, computerScore);
}

function playerMoveChoice(){
    let playerMove = "";
    while (!possibleMoves.includes(playerMove)) {
        playerMove = prompt("Rock, paper or Cissors ?").toLowerCase();
    }
    return playerMove;
}

function computerMoveChoice(){
    return possibleMoves[randomIntFromInterval(0, possibleMoves.length - 1)];
}

function playRound(playerMove, computerMove){
    if(playerMove === computerMove){
        return undefined;
    }

    return ( (playerMove === "rock" & computerMove === "cissors") 
        | (playerMove === "paper" & computerMove === "rock") 
        | (playerMove === "cissors" & computerMove === "paper"))
}

function displayResult(playerScore, computerScore){

    console.log("Result : player : " + playerScore + " - computer : " + computerScore);
    if(playerScore > computerScore){
        console.log("Player wins !");
    }
    else if(playerScore < computerScore){
        console.log("Computer wins !");
    }
    else{
        console.log("It's a Draw !");
    }
}

// Utils
function randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min)
}